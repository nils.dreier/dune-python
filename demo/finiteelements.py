import numpy

from dune.grid import cartesianDomain, gridFunction
from dune.alugrid import aluConformGrid as grid
from dune.geometry import quadratureRule, referenceElement

from dune.istl import BuildMode, bcrsMatrix, blockVector, CGSolver, SeqJacobi

class LinearShapeFunction:
    def __init__(self, ofs, grad):
        self.ofs = ofs
        self.grad = grad
    def evaluate(self, local):
        return self.ofs + sum([x*y for x, y in zip(self.grad, local)])
    def gradient(self, local):
        return self.grad


def P1ShapeFunctionSet(dim):
    sfs = [LinearShapeFunction(1.0, [-1.0] * dim)]
    for i in range(dim):
        sfs.append(LinearShapeFunction(0.0, [float(i == j) for j in range(dim)]))
    return sfs


def assemble(gridView, f):
    dim = gridView.dimension

    basis = P1ShapeFunctionSet(dim)
    indexSet = gridView.indexSet

    numDofs = indexSet.size(dim)
    matrix = bcrsMatrix((numDofs, numDofs), 6, 0.1, BuildMode.implicit)
    rhs = blockVector(numDofs)

    for e in gridView.elements:
        geo = e.geometry

        gt  = e.type
        if not gt.isSimplex:
            raise NotImplementedError("finite element shape functions only implemented for simplex elements")

        for p in quadratureRule(gt, 1):
            x = p.position
            w = p.weight * geo.integrationElement(x)

            jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
            grads = [numpy.dot(jit, phi.gradient(x)) for phi in basis]

            indices = indexSet.subIndices(e, dim)
            for dphi, row in zip(grads, indices):
                for dpsi, col in zip(grads, indices):
                    matrix[row, col] += [[numpy.dot(dphi, dpsi) * w]]

        for p in quadratureRule(gt, 2):
            x = p.position
            w = p.weight * geo.integrationElement(x)
            for phi, i in zip(basis, indexSet.subIndices(e, dim)):
                rhs[i] += [phi.evaluate(x) * f(geo.toGlobal(x)) * w]
    matrix.compress()

    # Dirichlet boundary conditions

    drows = set()
    for i in gridView.boundaryIntersections:
        e = i.inside
        subs = referenceElement(e).subEntities((i.indexInInside, 1), dim)
        indices = indexSet.subIndices(e, dim)
        drows.update(indices[k] for k in subs)

    drows = numpy.array(list(drows))
    for r in drows:
        matrix[r] *= 0.0
        matrix[r, r] = [[1.0]]
        rhs[r] = [0.0]

    return matrix, rhs

domain = {'vertices': numpy.array([(0.0, 0.0), (1.0, 0.0), (1.0, 1.0), (0.0, 1.0)]), 'simplices': numpy.array([(0, 1, 2), (0, 2, 3)])}
gridView = grid(domain)
gridView.hierarchicalGrid.globalRefine(8)

print("Assembling...")
matrix, rhs = assemble(gridView, lambda v: sum(2.0 * x * (1 - x) for x in v))

print("Solving...")
u = blockVector(gridView.indexSet.size(2))
CGSolver(matrix.asLinearOperator(), SeqJacobi(matrix), 1e-10)(u, rhs)

print("Visualizing...")
dim = gridView.dimension
basis = P1ShapeFunctionSet(dim)
@gridFunction(gridView,"u_h")
def u_h(e, x):
    return [sum(phi.evaluate(x) * u[i][0] for phi, i in zip(basis, gridView.indexSet.subIndices(e, dim)))]
gridView.writeVTK("fem2d", pointdata=[u_h])
